'use strict';

$(document).ready(function() {
	var searchButton = $('.fa-search'),
		sortButton = $('.fa-sort'),
		searchBar = $('.search-bar'),
		inputs = $( '.input-file' );

	searchButton.click(function() {
		$('.header__functions i').addClass('button-hidden');
		searchBar.addClass('search-bar-open');
	});

	searchBar.keydown(function (e) {
		if (e.which == 13) {
			e.preventDefault();
			$('.header__functions i').removeClass('button-hidden');
			searchBar.removeClass('search-bar-open');
		}
	});

	sortButton.click(function () {
		$('.sort-options').toggleClass('visible');
	});

	$('.sort-options li').click(function() {
		$('.active').removeClass('active');
		$(this).addClass('active');
	});

	$('.login').click(function() {
		$('.show').removeClass('show');
		$('[data-form="login"]').addClass('show');
	});

	$('.signup').click(function() {
		$('.show').removeClass('show');
		$('[data-form="signup"]').addClass('show');
	});

	$('.photo-pane').click(function() {
		$('.show').removeClass('show');
		$('.modal--lightbox').addClass('show');
	});

	$('.close-button').click(function() {
		$('.show').removeClass('show');
	});
    
    
    $('.input-file').change(function(e) {
        var fileName = '',
            label = this.nextElementSibling,
            labelValue = label.innerHtml;
        
        if (this.files && this.files.length > 1) {
            fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        } else {
            fileName = e.target.value.split('\\').pop();
        }
        
        if (fileName) {
            label.querySelector('span').innerHTML = fileName;
        } else {
            label.innerHtml = labelValue;
        }
    });
    
    $.ajax({
            type: 'GET',
            url: 'http://127.0.0.1:8080/Testt/webresources/entity.image',
            dataType: 'xml',
            success: function(xml) {
                var imagePathList = [],
                    photoPaneCount = $('.photo-pane img').length;
                $(xml).find('image').each(function () {
                    var path = $(this).find('ipath').text();
                    imagePathList.push(path);
                });
                var i = 0;
                while (i < photoPaneCount && i < imagePathList.length) {
                    $('.photo-pane img').eq(i).attr("src", "http://127.0.0.1:7791/upload/" + imagePathList[i]);
                    i++;
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert('ends up in error state');
            }
        });
});
